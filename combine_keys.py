import key as k
import coins

all_coins = [coin for coin in dir(coins) if not '_' in coin]
sel = None

while not sel in all_coins:
    sel = input('Coin (full name): ')

net_vers = getattr(coins, sel)[0]
privkey_pre = getattr(coins, sel)[1]

done = False
while fail:
    try:
        servpriv = int(input('Server privkey: '), 16)
        userpriv = int(input('Your privkey: '), 16)
        vanitypriv = servpriv + userpriv
        addr, rawpub, wif, rawpriv = k.make_address(vanitypriv.to_bytes(32, 'big'), net_vers, privkey_pre)
        print("Public key: " + addr)
        print("Privkey: " + wif)
        done = True
    except Exception as E:
        print(E)
        exit()
