from ecdsa import SigningKey, SECP256k1
import key as k
import coins

all_coins = [coin for coin in dir(coins) if not '_' in coin]
sel = None

while not sel in all_coins:
    sel = input('Coin (full name): ')

net_vers = getattr(coins, sel)[0]
privkey_pre = getattr(coins, sel)[1]

sk = SigningKey.generate(curve=SECP256k1)
vk = sk.get_verifying_key()
addr, rawpub, wif, rawpriv = k.make_address(sk.to_string(), net_vers, privkey_pre)
print('Public key (compressed): ' + addr)
print('Privkey (compressed): ' + wif)
print('================================')
print('Give this public key to the vanity server.')
print('Public key (raw): ' + rawpub)
print('================================')
print('Use this in combine_keys.py')
print('Privkey (raw): ' + rawpriv)
